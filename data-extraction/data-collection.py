import requests
import json
import time
from urllib.parse import urlparse
import csv

current_milli_time = lambda: int(round(time.time() * 1000))

def fetch_bad_urls():
    """
    Fetches all bad urls, makes sure they only include protocl and host
    and ensures that there are no duplicates
    """
    response = requests.get('http://data.phishtank.com/data/online-valid.json')
    url_list = list(set(list(map(lambda item: get_base_url(item["url"]), json.loads(response.text)))))
    return url_list[0:1000]

def get_base_url(fullUrl):
    """
    Given a full path URL this function will return 
    the base url only. (Protocol and host)
    For example:
    - Given : https://example.com/example/path
    - Returned value : https://example.com/
    """
    parsed_uri = urlparse(fullUrl)
    result = '{uri.scheme}://{uri.netloc}/'.format(uri=parsed_uri)
    return result


def search_for_term(term):
    """
    Run a search usign Bing Web API to get search results for passed "term"
    Each search perfromed will return 50 search results which is cleaned and filtered to remove duplicates
    """
    headers = {'Ocp-Apim-Subscription-Key': 'aa4948d5df3e4eb9be6e59ee362f62bf'}
    response = requests.get("https://api.cognitive.microsoft.com/bing/v7.0/search?count=50&responseFilter=Webpages&q={term}",headers=headers)
    response_json = json.loads(response.text)
    values_list = response_json["webPages"]["value"]
    url_list = list(map(lambda item: get_base_url(item["url"]),values_list))
    return url_list

def fetch_good_urls():
    """
    Fetches a list of good urls by perfroming web searchs on numbers from 0 to 100
    """
    url_lists = []
    for i in range(100):
        search_result = search_for_term(i)
        url_lists.append(list(set(search_result)))
        time.sleep(1)
    return [item for sublist in url_lists for item in sublist]

bad_urls = fetch_bad_urls()
good_urls = fetch_good_urls()
# Writes data to CSV file
with open('url_collection.csv', 'w') as csvfile:
    fieldnames = ['url', 'type']
    writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
    writer.writeheader()
    for url in bad_urls:
        writer.writerow({'url': url, 'type': 'bad'})
    for url in good_urls:
        writer.writerow({'url': url, 'type': 'good'})