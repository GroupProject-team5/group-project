## Overview
The python code included `data-collectio.py` will generate a **CSV** file title `url_collection.csv`.

This code needs **Python 3.x** and [Python Requests](http://docs.python-requests.org/en/master/user/quickstart/) to run.

See **how to run** section for instructions.
## How to run
```
pip3 install virtualenv
virtualenv venv
source venv/bin/activate
pip3 install requests
python3.7 data-collection.py
```