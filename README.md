# Group Project - Team 5

## Detecting Malicious URLs using Machine Learning

### Project aim
Developing a **python** program which builds two predictive models to classify URLs. The classification is between two categories: 
1. Good Url 
2. Malicious (Bad) Url. 
 
The two models used are **Multi-layer perceptron** and **Random Forest**.

More project detail can be found in [Wiki](https://gitlab.com/GroupProject-team5/group-project/wikis/home#detecting-malicious-urls-using-machine-learning)

## Team details

### Team member
* Mohammed Alshamrani
* Xinyu Fan
* Jie Jia
* Dongming Mao
* Satoshi Nakamura
* Kyle Quinn

### Division of work
* **Data Collection**: Mohammed Alshamrani
* **Model Building**: Xinyu Fan, Dongming Mao, Jie Jia, Satoshi Nakamura
* **Evaluation**: Kyle Quinn

## Project plan

The steps required to complete the project are:
### 1. Dataset collection: Mohammed A. A. Alshamrani

First we need to create a large collection (set) of URLs which contains a mix of both malicious and non-malicious URL’s. The collection should not have any duplicates or subdomains and will be in the form of a CSV file. 

  

To do this we will create a python script that makes use of two different API’s, one to gather malicious URL’s and the other non-malicious: 


*  Malicious URL’s 

    *  These URL’s will be collected by using the http://www.phishtank.com/developer_info.php API, which contains an updated list of known phishing URL’s  

*  Non-malicious URL’s 

    *  These URL’s will be collected by using the Bing Search API (https://docs.microsoft.com/en-us/azure/cognitive-services/bing-web-search/quickstarts/python) to perform search queries to find good URL’s. This is done by; 

        1.  First, a number between 0-9999 will be randomly generated and then used to perform a search. 


        2.  Then the subsequent set of URL’s found will then be placed into a dataset, where duplicate and subdomain URL’s have been removed and all URL’s have been sanitized.  


    *  We can assume that most page 1 results are considered non-malicious URL’s as it can be assumed that Bing has already vetted these URL’s to check whether they are malicious or not 

### 2. Model Building and Tool Creation: Xinyu Fan, Jie Jia, Dongming Mao, Satoshi  Nakamura
It generally consists of two parts:
*  Data sets
*  Machine Learning Model

We are aiming to find the feature of URLs and make URLs into data frames, and then dividing the data into training sets and test sets for machine learning. We are going to use Multi-Layer Perceptron and Random Forest as our machine learning methods.

**Multi-Layer Perceptron**

Multi-layered Perceptron models is a model that consists three layers or more of nodes starting with the input layer, hidden layer and an outer layer. Also its a feedforward artificial neural network that generates a set of outputs that come from the set of inputs on the first layer of the model. The nodes represent the neurons within the model , each node has a nonlinear activation function as well. This model is most commonly utilized for training data within a program. In this project , we will be training the model to analyze the dataset we have to identify which URLS are malicious and which are not.

The progress of building the Multi-layer Perceptron model is as follows: 1) All the inputs are placed at the first layer. 2) We assume each sample that we have X attributes , we implement those into the hidden layer. 3) repeat step 2 to increase number of nodes (neurons) until we have the amount we want. 4) The data will then be trained at the outer layer. 5) loop step 1-4 to generate data. 6) Classification is done, Malicious URLs identified. 

**Random Forests**
 
Random forests are a method widely used in machine learning. It creates N different independent decision trees. When the forest gets a new sample, it will make the sample goes through all the trees and generate N results. And the result which occurs most frequently will become the output of the forest. In this very project, we input many URLs, analyse the data, find their key attributes, and use random forests to identify malicious URLs.
 
The progress of building a random forests tree is as follows: 1) from all the inputs, we use Bootstrapping to choose N samples. 2) we assume that every sample has M attributes, and we choose only one attribute to split the father node into two child nodes. 3) keep doing step 2) until the Gini impurity is low enough. 4) loop step 1)-3) until it generates a large number of decision trees.

### 3. Evaluation and Conclusion: Kyle A. Quinn
Evaluate: 

  

In order to produce a meaningful conclusion, we must evaluate the performance of the program that we have created by measuring the most important variable, the accuracy of training and the success rate of the model created. This will be done by using the validation dataset, calculating the success rate of the model in correctly identifying any malicious URL’s. 

This variable will produce different results with respect to some parameters including; 


1.  Choosing different hyper-parameters 


    *  This will could affect the accuracy of training and will be done by choosing different hyper-parameters and re-training the program 


2.  Changing the size of the dataset 


    *  This could also affect the accuracy of training and will be done by choosing a subset of the original dataset and again re-training the program to produce another model that can then be evaluated 


3.  Whether or not the model is Multi-Layer Perceptron or a Random Forest 


    *  This will also affect the performance of the training as they are different algorithms to train the program and will hence produce different performance results 

 

The different performances of each model that have been produced, by changing the parameters, will then be compared. This comparison will then be used to draw conclusions from the project. 

 

The conclusion will include a summary of the main points that have been addressed, such as the model, paired with its optimal parameters, that produces the highest success rate in detecting malicious URL’s. The goal will be assessed to see if it has been reached and any limitations that may have arisen during the project. The conclusion will also include any extensions to the project that could be made. 

